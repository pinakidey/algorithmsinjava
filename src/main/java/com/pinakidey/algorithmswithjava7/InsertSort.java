/*
 * Copyright 2016 Pinaki.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pinakidey.algorithmswithjava7;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Pinaki
 */
public class InsertSort {

    /**
     * Quick-sort method
     *
     * <pre>
     * Given a list l, and a new list nl
     * for each element originallistelem in list l:
     * for each element newlistelem in list nl:
     * if (originallistelem < newlistelem):
     * insert originallistelem in nl before newlistelem
     * else move to the next element
     * if originallistelem has not been inserted:
     * insert at end of nl
     * </pre>
     * @see http://bigocheatsheet.com/ for time-space complexity 
     * @param numbers list of input
     * @return list of sorted numbers
     */
    public static List<Integer> insertSort(final List<Integer> numbers) {
        final List<Integer> sortedList = new LinkedList<>();
        originalList:
        for (Integer number : numbers) {
            for (int i = 0; i < sortedList.size(); i++) {
                if (number < sortedList.get(i)) {
                    sortedList.add(i, number);
                    continue originalList;
                }
            }
            sortedList.add(sortedList.size(), number);
        }
        return sortedList;
    }
}
