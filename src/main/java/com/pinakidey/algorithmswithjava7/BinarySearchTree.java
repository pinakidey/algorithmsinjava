/*
 * Copyright 2016 Pinaki.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pinakidey.algorithmswithjava7;

/**
 *
 * @author Pinaki
 * @param <E>
 */
public class BinarySearchTree<E extends Comparable> {

    private E value;
    private BinarySearchTree<E> left;
    private BinarySearchTree<E> right;

    public BinarySearchTree() {
    }

    public BinarySearchTree(E value, BinarySearchTree<E> left, BinarySearchTree<E> right) {
        this.value = value;
        this.left = left;
        this.right = right;
    }

    public E getValue() {
        return value;
    }

    public void setValue(E value) {
        this.value = value;
    }

    public BinarySearchTree<E> getLeft() {
        return left;
    }

    public void setLeft(BinarySearchTree<E> left) {
        this.left = left;
    }

    public BinarySearchTree<E> getRight() {
        return right;
    }

    public void setRight(BinarySearchTree<E> right) {
        this.right = right;
    }

    /**
     * Search Algorithm in BST
     *
     * <pre>
     * If the value to find is equal, we have a match. If the value to find is less than the current node, and
     * the left node is not null, repeat the search on the left node. Otherwise, if the right node is not null,
     * repeat the search on the right node. If a child node to search is null, we have reached the bottom of
     * the tree, and the value to find is not in the tree.
     * </pre>
     *
     * @param toFind
     * @return
     */
    public boolean search(final E toFind) {
        if (toFind.equals(value)) {
            return true;
        }
        if (toFind.compareTo(value) < 0 && left != null) {
            return left.search(toFind);
        }
        return right != null && right.search(toFind);
    }

    /**
     * Insert Algorithm in BST
     *
     * <pre>
     * Follow the tree, comparing each node with the value to insert, 
     * going left if the value to insert is less than the value at the current node, or
     * right otherwise. If the next node to inspect is null, this is where new value is to be inserted.
     * </pre>
     *
     * @param toInsert
     */
    public void insert(final E toInsert) {
        if (toInsert.compareTo(value) < 0) {
            if (left == null) {
                left = new BinarySearchTree<>(toInsert, null, null);
            } else {
                left.insert(toInsert);
            }
        } else if (right == null) {
            right = new BinarySearchTree<>(toInsert, null, null);
        } else {
            right.insert(toInsert);
        }
    }
}
