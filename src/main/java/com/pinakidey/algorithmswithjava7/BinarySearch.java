/*
 * Copyright 2016 Pinaki.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pinakidey.algorithmswithjava7;

import java.util.List;

/**
 *
 * @author Pinaki
 */
public class BinarySearch {

    /**
     * Binary Search Method
     *
     * @param numbers list of sorted numbers
     * @param value value to be searched in the list
     * @return
     */
    public static boolean binarySearch(final List<Integer> numbers,
            final Integer value) {
        if (numbers == null || numbers.isEmpty()) {
            return false;
        }
        final Integer comparison = numbers.get(numbers.size() / 2);
        if (value.equals(comparison)) {
            return true;
        }
        if (value < comparison) {
            return binarySearch(
                    numbers.subList(0, numbers.size() / 2),
                    value);
        } else {
            return binarySearch(
                    numbers.subList(numbers.size() / 2 + 1, numbers.size()),
                    value);
        }
    }
}
