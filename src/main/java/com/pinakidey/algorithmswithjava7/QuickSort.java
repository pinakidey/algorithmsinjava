/*
 * Copyright 2016 Pinaki.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pinakidey.algorithmswithjava7;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Pinaki
 */
public class QuickSort {

    /**
     * Quick-sort method
     *
     * <pre>
     * method quicksort(list l):
     * if l.size < 2:
     * return l
     * let pivot = l(0)
     * let lower = new list
     * let higher = new list
     * for each element e in between l(0) and the end of the list:
     * if e < pivot:
     * add e to lower
     * else add e to higher
     * let sortedlower = quicksort(lower)
     * let sortedhigher = quicksort(higher)
     * return sortedlower + pivot + sortedhigher
     * </pre>
     * @see http://bigocheatsheet.com/ for time-space complexity
     * @param numbers List of numbers to be sorted
     * @return sorted list of numbers
     */
    public static List<Integer> quicksort(List<Integer> numbers) {
        if (numbers.size() < 2) {
            return numbers;
        }
        final Integer pivot = numbers.get(0);
        final List<Integer> lower = new ArrayList<>();
        final List<Integer> higher = new ArrayList<>();
        for (int i = 1; i < numbers.size(); i++) {
            if (numbers.get(i) < pivot) {
                lower.add(numbers.get(i));
            } else {
                higher.add(numbers.get(i));
            }
        }
        final List<Integer> sorted = quicksort(lower);
        sorted.add(pivot);
        sorted.addAll(quicksort(higher));
        return sorted;
    }
}
