package com.pinakidey.algorithmswithjava7;

/**
 *
 * @author Pinaki
 */
public class BubbleSort {

    /**
     * Bubble Sort Method
     * <pre>
     * for i between 0 and (array length – 2):
     *  if (array(i + 1) < array(i)):
     *      switch array(i) and array(i + 1)
     * </pre>
     * @see http://bigocheatsheet.com/ for time-space complexity
     * @param numbers array of numbers to be sorted
     */
    public static int[] bubbleSort(int[] numbers) {
        boolean numbersSwitched;
        do {
            numbersSwitched = false;
            for (int i = 0; i < numbers.length - 1; i++) {
                if (numbers[i + 1] < numbers[i]) {
                    int tmp = numbers[i + 1];
                    numbers[i + 1] = numbers[i];
                    numbers[i] = tmp;
                    numbersSwitched = true;
                }
            }
        } while (numbersSwitched);
        
        return numbers;
    }
}
