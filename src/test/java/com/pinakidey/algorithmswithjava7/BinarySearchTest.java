/*
 * Copyright 2016 Pinaki.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pinakidey.algorithmswithjava7;

import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pinaki
 */
public class BinarySearchTest {

    public BinarySearchTest() {
    }

    /**
     * Test of binarySearch method, of class BinarySearch.
     */
    @Test
    public void testBinarySearch() {
        System.out.println("binarySearch");
        final List<Integer> numbers = Arrays.asList(4, 7, 1, 6, 3, 5, 4);
        final int valueToFind = 5;
        assertEquals(true, BinarySearch.binarySearch(MergeSort.mergesort(numbers), valueToFind));
    }
    
    /**
     * Test of binarySearch method, with unsorted array
     */
    @Test
    public void testBinarySearchWithUnsortedArray() {
        System.out.println("binarySearchWithUnsortedArray");
        final List<Integer> numbers = Arrays.asList(4, 7, 1, 6, 3, 5, 4);
        final int valueToFind = 5;
        assertEquals(false, BinarySearch.binarySearch(numbers, valueToFind));
    }
}
