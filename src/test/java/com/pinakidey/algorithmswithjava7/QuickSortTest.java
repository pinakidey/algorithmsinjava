/*
 * Copyright 2016 Pinaki.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pinakidey.algorithmswithjava7;

import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pinaki
 */
public class QuickSortTest {

    public QuickSortTest() {
    }

    /**
     * Test of quicksort method, of class QuickSort.
     */
    @Test
    public void testQuicksort() {
        System.out.println("quicksort");
        final List<Integer> numbers = Arrays.asList(4, 7, 1, 6, 3, 5, 4);
        final List<Integer> expected = Arrays.asList(1, 3, 4, 4, 5, 6, 7);

        assertEquals(expected, QuickSort.quicksort(numbers));
    }
    
    /**
     * Test quicksort with empty list
     */
    @Test
    public void testQuicksortWithEmptyList() {
        System.out.println("quicksortWithEmptyList");
        final List<Integer> numbers = Arrays.asList();
        final List<Integer> expected = Arrays.asList();

        assertEquals(expected, QuickSort.quicksort(numbers));
    }

}
