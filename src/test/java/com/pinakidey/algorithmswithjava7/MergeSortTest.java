/*
 * Copyright 2016 Pinaki.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pinakidey.algorithmswithjava7;

import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pinaki
 */
public class MergeSortTest {

    public MergeSortTest() {
    }

    /**
     * Test of mergesort method, of class MergeSort.
     */
    @Test
    public void testMergesort() {
        System.out.println("mergesort");
        final List<Integer> numbers = Arrays.asList(4, 7, 1, 6, 3, 5, 4);
        final List<Integer> expected = Arrays.asList(1, 3, 4, 4, 5, 6, 7);

        assertEquals(expected, MergeSort.mergesort(numbers));
    }

    /**
     * Test of mergesort method, with empty list.
     */
    @Test
    public void testMergesortWithEmptyList() {
        System.out.println("mergesortWithEmptyList");
        final List<Integer> numbers = Arrays.asList();
        final List<Integer> expected = Arrays.asList();

        assertEquals(expected, MergeSort.mergesort(numbers));
    }

    /**
     * Test of mergesort method, with reverse-sorted list.
     */
    @Test
    public void testMergesortWithReverseSortedList() {
        System.out.println("mergesortWithReverseSortedList");
        final List<Integer> numbers = Arrays.asList(9, 8, 7, 6, 5, 4, 3, 2, 1);
        final List<Integer> expected = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);

        assertEquals(expected, MergeSort.mergesort(numbers));
    }

}
