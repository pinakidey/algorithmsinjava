/*
 * Copyright 2016 Pinaki.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pinakidey.algorithmswithjava7;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pinaki
 */
public class BinarySearchTreeTest {

    public BinarySearchTreeTest() {
    }    

    /**
     * Test of insert method, of class BinarySearchTree.
     */
    @Test
    public void testInsert() {
        System.out.println("insert");
        final BinarySearchTree<Integer> root = new BinarySearchTree<>(7, null, null);
        root.insert(3);
        root.insert(9);
        root.insert(10);
        assertTrue(root.search(10));
        assertEquals(Integer.valueOf(10),
                root.getRight().getRight().getValue());
    }
}
